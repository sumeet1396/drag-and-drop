window.onload = () => {

	const enableDragSort = listClass => {
		const sortableLists = document.querySelectorAll(listClass);
		Array.prototype.map.call(sortableLists, (list) => {enableDragItem(list);});
	}

	function enableDragItem(item) {
		item.ondrag = handleDrag;
		item.ondragend = handleDrop;
	}

	function handleDrag(item) {
		const selectedItem = item.target,
					list = selectedItem.parentNode,
					x = event.clientX,
					y = event.clientY;

		console.log(selectedItem);
		selectedItem.classList.add('drag-li');
		let swapItem = document.elementFromPoint(x, y) === null ? selectedItem : document.elementFromPoint(x, y);
		console.log(swapItem);
		if (list === swapItem.parentNode) {
			swapItem = swapItem !== selectedItem.nextSibling ? swapItem : swapItem.nextSibling;
			list.insertBefore(selectedItem, swapItem);
		}
	}

	function handleDrop(item) {
		item.target.classList.remove('drag-li');
	}

	(()=> {enableDragSort('.list-container li')})();
}